provider "aws" {
  region  = "us-west-2"
  version = "~> 4.13.0"
}
resource "aws_instance" "web" {
  ami             = "ami-0ca285d4c2cda3300"
  instance_type   = "t2.micro"
  key_name        = "switchninja"
  security_groups = [aws_security_group.web.name]

  tags = {
    "Name" = "WebServerByTf"
  }
}

resource "aws_security_group" "web" {
  name        = "web-security-group"
  description = "Allow access to our web server"

  ingress {
    description = "Allow SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["173.14.252.224/28"]
  }
}

output "instance_public_dns" {
  value = aws_instance.web.public_dns
}